import { Token } from '@/types/data'

const TOKENKEY: string = 'geek-h5-token'

// 取
const getToken = (): Token => JSON.parse(localStorage.getItem(TOKENKEY) ?? '{}')

// 存
const setToken = (token: Token): void =>
  localStorage.setItem(TOKENKEY, JSON.stringify(token))

// 清
const clearToken = () => localStorage.removeItem(TOKENKEY)

// 是否登录
const isAuth = () => !!getToken().token

export { isAuth, getToken, setToken, clearToken }
